---
title: About CivicTechGbg
position: 1
description: Hackathons for the Civic Tech community of Gothenburg
ref: about
---

## Background
There are many non-profits and movements in Gothenburg working for social and environmental goals and making use of digital technologies. There’s also a lot of knowledge and innovative ideas in the city. By organising a series of hackathons together with different themes we wish to raise the status for the civic tech sector and bring together developers, designers, hackers, activists, innovators and anyone sharing the same interests and values.

## Hackathons
A series of hackathons to promote civic-tech development is organised by different actors from civil society and the municipality. Some of our inspiration comes from Code for Germany and Open Democracy Now in France.

The hackathons will happen every other month in average and each will last a whole day during the weekend. Each hackathon has a theme and is organised by different partners. There should be 2 to 4 ongoing projects in each theme that participants can choose from. These projects are prepared and lead by the respective organising partners. It’s still possible to register your own project in advance if it fits the theme and follows the guidelines.

The partners responsible for the organisation of each hackathon should collaborate in reaching out to the public, finding participants within and outside their own networks, presenting their projects and leading group participants during the event.

Digidem Lab is supporting by providing the venue, food (also the famous Swedish fika) and guidance and (under specific circumstances).

## Guidelines

> **1) The project should be about developing or using new technologies in order to promote an increased participation in social movements, non-profit associations, democratic political processes and civil society in general.**

> **2) The results of the project should be made available to the public.**

* The workflow and process in the project can be documented so that more people can learn from it.
* Any code generated should use an open-source license.
* Text, pictures or videos produced should be licensed under Creative Commons.
