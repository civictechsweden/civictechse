---
title: CivicTechGbg
ref: start
description: Hackathons for the Civic Tech community of Gothenburg
layout: start
news-flash:
- text: In CivicTechGbg developers, designers, activists and open data enthusiasts
    come together to build applications, visualisations and tools for citizens. Read
    about our upcoming hackathons and get in touch to join the network!
  link-title: About us
  link: "/en/about"
---

