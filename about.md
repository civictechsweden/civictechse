---
title: Om CivicTechGbg
position: 1
description: Hackathons för Civic Tech-communitiet i Göteborg.
ref: about
---

## Bakgrund
Det finns flera ideella organisationer i Göteborg som jobbar med digital teknik för samhällsnytta. Här finns väldigt mycket kunskap och innovativa idéer. Genom att arrangera gemensamma hackathons med olika teman kan vi höja statusen för civic tech och bygga upp ett gemensamt community av utvecklare, designers, aktivister och andra intresserade.

## Hackathons
En serie hackathons för att främja civic tech-utvecklingen i Göteborg arrangeras av olika aktörer från civilsamhället och staden. Inspirationen kommer från bland annat franska Open Democracy Now och tyska Code for Germany.

> **Hackathon ordnas ungefär varannan månad, en heldag på helgen.**

Varje hackathon har ett tema och ordnas i samarbete mellan några olika organisationer. Inom varje tema finns några förberedda projekt som samarbetspartnerna ansvarar för, så att varje hackathon har 2-4 projekt som deltagarna kan välja mellan. Det går också att anmäla egna projekt i förväg som passar in på temat och riktlinjerna.

> **Deltagande organisationer hjälps åt att samla folk och sprida eventet, introducera projekten och handleda sin projektgrupp.**

Digidem Lab kan bidra med lokaler, fika och mat, handledning/processledning och eventuellt arvode till externa handledare.

## Riktlinjer
> **1) Projektet ska främja ökat deltagande i sociala rörelser, föreningsliv, politiska processer eller samhället i stort med hjälp av ny teknik.**

> **2) Projektresultatet ska göras tillgängligt för alla.**

* Arbetssättet dokumenteras så att fler kan dra nytta av lärdomarna från projektet.
* Kod som produceras använder någon av licenserna för öppen källkod.
* Text, bild eller video som produceras använder någon av Creative Commons licenser.
