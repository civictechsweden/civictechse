---
title: Sharing Is Caring
event-date: 2017-10-28 00:00
event-date-desc: 28 oktober 2017 (preliminärt)
place: Göteborg
ref: sharing
excerpt: Kollaborativ ekonomi, gemensamma resurser och hållbar konsumtion.
ingress-text: Smarta lösningar för ett hållbart samhälle. Donec ullamcorper nulla
  non metus auctor fringilla. Curabitur blandit tempus porttitor.
links:
- title: Anmälan
  url: "#"
  style: button
projects:
- title: Bygg en delningsplattform i Yunity
  text: Nullam quis risus eget urna mollis ornare vel eu leo. Sed posuere consectetur
    est at lobortis. Praesent commodo cursus magna, vel scelerisque nisl consectetur
    et. Maecenas sed diam eget risus varius blandit sit amet non magna. Fusce dapibus,
    tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo
    sit amet risus. Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod.
  image: "/uploads/yunity-orange.svg"
  links:
  - title: Läs om Yunity
    url: https://yunity.org/en
- title: Gör Smarta Kartan open source
  text: Etiam porta sem malesuada magna mollis euismod. Morbi leo risus, porta ac
    consectetur ac, vestibulum at eros. Vivamus sagittis lacus vel augue laoreet rutrum
    faucibus dolor auctor. Nullam quis risus eget urna mollis ornare vel eu leo. Donec
    ullamcorper nulla non metus auctor fringilla. Etiam porta sem malesuada magna
    mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit.
  image: "/uploads/smarta-kartan-logo.svg"
  links:
  - title: Smartakartan.se
    url: http://smartakartan.se/
partner-text: Sharing is Caring arrangeras av Digidem Lab och Solidariskt Kylskåp
  Vestibulum id ligula porta felis euismod semper.
partner-links:
- name: Solidariskt Kylskåp
  logo: "/uploads/logo-solikyl.jpg"
  link: http://solikyl.se/
- name: Smarta Kartan
  logo: "/uploads/logo-solikyl.jpg"
  link: http://smartakartan.se/
---

Collaborative economy, the real sharing economy, the commons and sustainable consumption. Etiam porta sem malesuada magna mollis euismod. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Etiam porta sem malesuada magna mollis euismod. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Etiam porta sem malesuada magna mollis euismod. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.
