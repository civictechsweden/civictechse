---
title: I Like To Move It
permalink: "/en/hackathon/i-like-to-move-it"
event-date: 2018-02-01 00:00
event-date-desc: februari 2018
place: Göteborg
ref: move
lang: en
links:
- title: Anmälan
  url: "#"
  style: button
image-top: "/uploads/move-it.gif"
excerpt: Hållbara transporter, resande och rörlighet.
ingress-text: Cras mattis consectetur purus sit amet fermentum. Aenean eu leo quam.
  Pellentesque ornare sem lacinia quam venenatis vestibulum.
projects:
- title: Skjutsgruppen goes open source
  text: Var med och gör en unik svenskt samåkningstjänst open source!
  image: "/uploads/logo-skjutsgruppen.jpg"
  links:
  - title: Skjutsgruppen
    url: http://www.skjutsgruppen.nu/
- title: Mobility as a service
  text: Blockchainbaserat resande FTW.
- title: Cykelköket
  text: Passa på att laga din cykel hos Cykelköket!
partner-text: I Like To Move It arrangeras av Ideella Järngänget Skjutsgruppen och
  vestibulum id ligula porta felis euismod semper.
partner-links:
- name: Ideella samåkningsrörelsen Skjutsgruppen
  logo: "/uploads/logo-skjutsgruppen.jpg"
  link: http://skjutsgruppen.se/
---

Cras justo odio, dapibus ac facilisis in, egestas eget quam. Curabitur blandit tempus porttitor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec ullamcorper nulla non metus auctor fringilla. Donec ullamcorper nulla non metus auctor fringilla. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.
