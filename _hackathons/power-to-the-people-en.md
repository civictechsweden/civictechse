---
title: Power To The People
permalink: "/en/hackathon/power-to-the-people"
event-date: 2017-12-09 00:00
event-date-desc: 9 december 2017 (preliminärt)
place: Göteborg
ref: power
lang: en
excerpt: Demokrati, deltagande och transparens.
ingress-text: Curabitur blandit tempus porttitor. Maecenas sed diam eget risus varius
  blandit sit amet non magna.
links:
- title: Anmälan
  url: "#"
  style: button
projects:
- title: Medborgarplattformen Consul
  text: Vi installerar, testar och översätter Consul, Madrids plattform för deltagarbudget
    och Europas mest använda medborgarplattform.
  image: "/uploads/consul-share.png"
  links:
  - title: Making Madrid´s citizen platform accessible to a wider audience
    url: https://medium.com/digidemlab/making-madrid-s-citizen-platform-accessible-to-a-wider-audience-f452dd59a394
- title: Visualisering av Göteborgs öppna data
  text: Alla inköp från Göteborgs stad finns som öppen data, vi labbar med Open Spending
    och andra verktyg för att visualisera den data som finns.
  image: "/uploads/logo-openspending.png"
  links:
  - title: Open Spending
    url: http://openspending.org/
  - title: Göteborgs öppna data
    url: http://data.goteborg.se/
- title: Folkomröstning med Blockchain
  text: Democracy Earth presenterar sitt projekt med blockchainbaserade omröstningar.
  image: "/uploads/logo-democracyearth2.png"
  links:
  - title: Democracy Earth
    url: http://democracy.earth/
partner-text: Power To The People arrangeras av Digidem Lab och vestibulum id ligula
  porta felis euismod semper.
partner-links:
- name: Digidem Lab
  logo: "/uploads/logo-digidemlab.png"
  link: http://digidemlab.org/
---

Curabitur blandit tempus porttitor. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Nulla vitae elit libero, a pharetra augue. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Donec ullamcorper nulla non metus auctor fringilla. Donec id elit non mi porta gravida at eget metus.
