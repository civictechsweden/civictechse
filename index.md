---
title: CivicTechGbg
ref: start
description: Hackathons för Civic Tech-communitiet i Göteborg
layout: start
news-flash:
- text: CivicTechGbg samlar utvecklare, designers, aktivister och öppen data-entusiaster
    som skapar tekniska lösningar för samhällsnytta. Läs om våra kommande hackathon
    och kontakta oss om du vill bli en del av vårt nätverk!
  link-title: Så funkar våra hackathon
  link: "/about"
---

